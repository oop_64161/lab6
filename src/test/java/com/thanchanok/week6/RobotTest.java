package com.thanchanok.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(10, robot.getX());
    }
    
    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRobotUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        Boolean result = robot.up();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldRobotUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        Boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }
 
    @Test
    public void shouldRobotUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        Boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        Boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotUpFailAtMin() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MIN);
        Boolean result = robot.up();
        assertEquals(false, result);
        assertEquals(Robot.Y_MIN, robot.getY());
    }

    @Test
    public void shouldRobotDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        Boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }


    @Test
    public void shouldRobotDownNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        Boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(14, robot.getY());
    }
 
    @Test
    public void shouldRobotDownNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        Boolean result = robot.down(10);
        assertEquals(true, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldRobotDownNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        Boolean result = robot.down(20);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldRobotDownBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.X_MAX - 1);
        Boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(Robot.X_MAX, robot.getY());
    }

    @Test
    public void shouldRobotDownFailAtMax() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX);
        Boolean result = robot.down();
        assertEquals(false, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }

    @Test
    public void shouldRobotLeftSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        Boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(9, robot.getX());
    }

    @Test
    public void shouldRobotLeftNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        Boolean result = robot.left(5);
        assertEquals(true, result);
        assertEquals(5, robot.getX());
    }

    @Test
    public void shouldRobotLeftNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        Boolean result = robot.left(10);
        assertEquals(true, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRobotLeftNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        Boolean result = robot.left(11);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRobotLeftBeforeMinSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN + 1, 9);
        Boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(Robot.X_MIN, robot.getX());
    }


    @Test
    public void shouldRobotLeftFailAtMin() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN, 9);
        Boolean result = robot.left();
        assertEquals(false, result);
        assertEquals(Robot.X_MIN, robot.getX());
    }


    @Test
    public void shouldRobotRightSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        Boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(11, robot.getX());
    }

    @Test
    public void shouldRobotRightNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        Boolean result = robot.right(5);
        assertEquals(true, result);
        assertEquals(15, robot.getX());
    }

    @Test
    public void shouldRobotRightNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        Boolean result = robot.right(9);
        assertEquals(true, result);
        assertEquals(19, robot.getX());
    }

    @Test
    public void shouldRobotRightNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        Boolean result = robot.right(12);
        assertEquals(false, result);
        assertEquals(19, robot.getX());
    }

    @Test
    public void shouldRobotRightBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX - 1, 11);
        Boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }


    @Test
    public void shouldRobotRightFailAtMax() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX, 11);
        Boolean result = robot.right();
        assertEquals(false, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }
}
