package com.thanchanok.week6;

public class BookBank {
    private String name;
    private double balance;
    public BookBank(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public Boolean deposit(double money) {
        if (money <= 0) {
            return false;
        }
        balance = balance + money;
        return true;
    }

    public Boolean withdraw(double money) {
        if (money <= 0) {
            return false;
        }
        if (balance < money) {
            return false;
        }
        balance = balance - money;
        return true;
    }

    public void print() {
        System.out.println(name + " " + balance);
    }

    public String getName() {
        return name;
    }

    public double getBalance () {
        return balance;
    }
}
