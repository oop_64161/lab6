package com.thanchanok.week6;

public class BookBankApp {
   public static void main(String[] args) {
       BookBank worawit = new BookBank("Worawit",100.0); //Default Constructor
       worawit.print();
       worawit.deposit(50);
       worawit.print();
       
       BookBank prayood =new BookBank("Prayoood",1);
       prayood.deposit(1000000);
       prayood.withdraw(10000000);
       prayood.print();
       
       BookBank praweeet = new BookBank("Praweeet",10.0);
       praweeet.deposit(10000000);
       praweeet.withdraw(1000000);
       praweeet.print();
   } 
}
